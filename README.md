infra
-----

-	전산실 관리

-	Xen server 관리

-	내부 서버 관리

-	방화벽 관리

-	마인즈랩 네트워크 구성도 ![네트워크 구성도](https://github.com/mindslab-ai/infra/blob/master/%EB%A7%88%EC%9D%B8%EC%A6%88%EB%9E%A9_%EC%82%AC%EB%82%B4_%EB%84%A4%ED%8A%B8%EC%9B%8C%ED%81%AC%EA%B5%AC%EC%84%B1%EB%8F%84_20170314.jpg)

-	에어컨 렌탈 업체 리마켓 / 고객센터 1544-0858 담당자 김영수 팀장 / 010-4116-0758 / 010-2261-2314

-	사내 네트워크 공사 업체 / 이온네트웍스 / 010-3219-2490
